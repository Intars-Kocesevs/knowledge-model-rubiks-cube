# knowledge model - Rubiks Cube



## Description
This project is a prototype / work-in-progress knowledge model for a subject of combinations puzzle known as Rubik's Cube. Knowledge models (also called an ontologies) intend to represent some knowledge in some field/domain - in this case it is a domain of this particular puzzle and relations that it has to other things/areas of life/fields of knowledges and human endeavours. For example, just as a chess game, so does the Rubik's Cube have its place in research in the field of artificial intelligence/machine learning and combinatorial mathematics as well as in art.

Since this Rubik's Cube' ontology follows OWL standart, it is shareable on computational level - and this is precisely one of the ultimate goals of this project --> to create a shareable and accessable structured knowledge for humans, bots and robots about this puzzle and what it brings with it topics-wise.

## Screenshots
![in form of UML diagrams; gaphor editor](<work in progress image - ontology UML sketch for Rubiks Cube topic (Intars, apr 2024).jpg>)
![OWL format ontology view in Protege](<work in progress image - ontology development for Rubiks Cube topic (Intars, may 2024)-2.jpg>)

## Installation
Project contains two main files:

- **Rubiks_Cube-ontology_lightweight_sketch.gaphor**  
  (sketch version of ontology, UML diagram format)

- **Rubiks_Cube-ontology_ver.1**  
  (full ontology /in development/, OWL standart)

A so called lightweight-sketch version can be opened/viewed/edit with free, open-source UML diagraming software "gaphor".
Sketch version is intended to be a way for fast, more compact access and experience of this Rubik's Cube ontology' exploration.

To open full-ontology, user needs to use free, open-source ontology editing/knowledge managing software "Protege" (it was developed by Stanford University Biomedical institute). 
https://protege.stanford.edu/

This main file can also be opened with some online browser-based ontology viewers/editors. One example would be an OWLGrEd - free online tool for some work with ontologies (OWLGrEd is made by researchers & developers from Latvia University).
http://owlgred.lumii.lv/

## Usage
In case of 'sketch'-ontology file, simply use gaphor, open file there.
In case of Rubik's_Cube-ontology_ver file use Protege editor > File > Open.
In case of using online tools it may be so that you just have to drag'n'drop any of both presented files to open them for some viewing experience.

## Support
kocesevs.intars@gmail.com

## Roadmap
From a prototype --> to an ever more fully and carefully crafted large scale ontology.

## Contributing
You can download offered files and edit them as you wish for yourself; here on this page currently - with project being in prototype mode, I do development/editions myself. Contributions (if any) can be accepted by me in textual communications format - you can send me some interesting facts or knowledge about this puzzle cube's nature, history, role in science and other fields of human endeavours to my email.

## Authors and acknowledgment
Intars Kocesevs


## Project status
prototype / work-in-progress
